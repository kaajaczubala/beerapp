import { Component } from '@angular/core';
import beers from './_files/beers.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'beer-app';
  checkedTheme: string;
  private brewerSelected1: string;
  private brewerSelected2: string;
  private brewerSelected3: string;
  brewers: string[] = [];
  beerInfo1: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  beerInfo2: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  loaded: any;
  beerInfo3: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  beerInfoSliced1: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  beerInfoSliced2: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  beerInfoSliced3: {
    price: number;
    name: string;
    image_url: string;
    type: string;
  }[] = [];
  Beers: {
    brewer: string;
    price: number;
    name: string;
    image_url: string;
    type: string;
    product_id: string | number;
  }[] = beers;
  currentIndex1 = 15;
  currentIndex2 = 15;
  currentIndex3 = 15;
  quantity: number;
  sortValue: string;
  beerValue1: string;
  beerValue2: string;
  beerValue3: string;

  ngOnInit() {
    this.checkedTheme = 'light';
    document.getElementById('btn1').style.visibility = 'hidden';
    document.getElementById('btn2').style.visibility = 'hidden';
    document.getElementById('btn3').style.visibility = 'hidden';
    for (let i = 0; i < this.Beers.length; i++) {
      if (!this.brewers.includes(this.Beers[i].brewer)) {
        this.brewers.push(this.Beers[i].brewer);
      }
    }
    this.brewers.unshift('');

    // localStorage.clear();
    let theme = localStorage.getItem("theme")
    let number = localStorage.getItem("number")
    let sort = localStorage.getItem("sort")
    let selected1 = localStorage.getItem("brewerSelected1")
    let selected2 = localStorage.getItem("brewerSelected2")
    let selected3 = localStorage.getItem("brewerSelected3")

    if (theme) {
      this.radioClick(theme);
      if (theme === 'dark') {
        this.checkedTheme = 'dark';
      } else if (theme === 'light') {
        this.checkedTheme = 'light';

      }
    }
    if (selected1) {
      this.beerValue1 = selected1;
      this.selectChangeHandler1(selected1);
    }
    if (selected2) {
      this.beerValue2 = selected2;
      this.selectChangeHandler2(selected2);
    }
    if (selected3) {
      this.beerValue3 = selected3;
      this.selectChangeHandler3(selected3);
    }
    if (number) {
      this.quantity = parseFloat(number);
      this.changeNumber(parseFloat(number));
    }
    if (sort) {
      this.sortValue = sort;
      this.sort(sort);
    }

  }

  sort(sortValue: string) {

    if (sortValue === 'Price') {
      this.beerInfo1 = this.beerInfo1.sort(this.dynamicSortNumber("price"));
      this.beerInfoSliced1 = this.beerInfo1.slice(0, this.currentIndex1);
      this.beerInfo2 = this.beerInfo2.sort(this.dynamicSortNumber("price"));
      this.beerInfoSliced2 = this.beerInfo2.slice(0, this.currentIndex2);
      this.beerInfo3 = this.beerInfo3.sort(this.dynamicSortNumber("price"));
      this.beerInfoSliced3 = this.beerInfo3.slice(0, this.currentIndex3);
    } else if (sortValue === 'Type') {
      this.beerInfo1 = this.beerInfo1.sort(this.dynamicSort("type"));
      this.beerInfoSliced1 = this.beerInfo1.slice(0, this.currentIndex1);
      this.beerInfo2 = this.beerInfo2.sort(this.dynamicSort("type"));
      this.beerInfoSliced2 = this.beerInfo2.slice(0, this.currentIndex2);
      this.beerInfo3 = this.beerInfo3.sort(this.dynamicSort("type"));
      this.beerInfoSliced3 = this.beerInfo3.slice(0, this.currentIndex3);
    } else if (sortValue === 'Name') {
      this.beerInfo1 = this.beerInfo1.sort(this.dynamicSort("name"));
      this.beerInfoSliced1 = this.beerInfo1.slice(0, this.currentIndex1);
      this.beerInfo2 = this.beerInfo2.sort(this.dynamicSort("name"));
      this.beerInfoSliced2 = this.beerInfo2.slice(0, this.currentIndex2);
      this.beerInfo3 = this.beerInfo3.sort(this.dynamicSort("name"));
      this.beerInfoSliced3 = this.beerInfo3.slice(0, this.currentIndex3);
    }
    localStorage.setItem("sort", sortValue);


  }
  changeNumber(quantity: number) {

    if (quantity >= 15 && quantity <= 30) {
      this.currentIndex1 = quantity;
      this.beerInfoSliced1 = this.beerInfo1.slice(0, this.currentIndex1);
      this.currentIndex2 = quantity;
      this.beerInfoSliced2 = this.beerInfo2.slice(0, this.currentIndex1);
      this.currentIndex3 = quantity;
      this.beerInfoSliced3 = this.beerInfo3.slice(0, this.currentIndex1);
      localStorage.setItem("number", quantity.toString());

    } else {
      alert("Value should be between 15 and 30")
    }

  }
  radioClick(theme: string) {

    const body = document.getElementById('body');
    const sec1 = document.getElementById('sec1');
    const sec2 = document.getElementById('sec2');
    const sec3 = document.getElementById('sec3');
    const panelOptions = document.getElementById('panelOptions');

    if (theme === 'light') {
      body.style.backgroundColor = 'rgb(238, 216, 176)'
      panelOptions.style.backgroundColor = 'rgb(238, 182, 109)'
      sec1.style.background = 'rgb(238, 182, 109)'
      sec2.style.background = 'rgb(238, 182, 109)'
      sec3.style.background = 'rgb(238, 182, 109)'
      localStorage.setItem("theme", theme);

    } else if (theme === 'dark') {
      body.style.backgroundColor = 'black'
      sec1.style.background = 'rgb(53, 64, 126)'
      sec2.style.background = 'rgb(53, 64, 126)'
      sec3.style.background = 'rgb(53, 64, 126)'
      panelOptions.style.backgroundColor = 'rgb(8, 7, 44)'
      localStorage.setItem("theme", theme);

    }

  }
  dynamicSortNumber(property: string) {

    let sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a: any, b: any) {
      let result = (parseFloat(a[property]) < parseFloat(b[property])) ? -1 : (parseFloat(a[property]) > parseFloat(b[property])) ? 1 : 0;
      return result * sortOrder;
    }
  }
  dynamicSort(property: string) {

    let sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a: any, b: any) {
      let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }
  showeHideBtn(id: string, listLen: number, maxLen: number) {
    let btn = document.getElementById(id);
    if (listLen < 15) {
      btn.style.visibility = 'hidden';
    } else {
      btn.style.visibility = 'visible';
    }
    if (listLen === maxLen) {
      btn.style.visibility = 'hidden';

    }
  }
  selectChangeHandler1(event: any) {


    if (event.target) {
      this.brewerSelected1 = event.target.value;
    } else {
      this.brewerSelected1 = event;
    }

    this.beerInfo1 = [];
    for (let i = 0; i < this.Beers.length; i++) {
      if (this.Beers[i].brewer === this.brewerSelected1) {
        this.beerInfo1.push({
          price: this.Beers[i].price,
          name: this.Beers[i].name,
          image_url: this.Beers[i].image_url,
          type: this.Beers[i].type,
        }
        )
      }
    }


    this.beerInfo1 = this.beerInfo1.sort(this.dynamicSort("name"));
    this.beerInfoSliced1 = this.beerInfo1.slice(0, this.currentIndex1);
    this.showeHideBtn('btn1', this.beerInfoSliced1.length, this.beerInfo1.length);
    localStorage.removeItem("brewerSelected1");
    localStorage.setItem("brewerSelected1", this.brewerSelected1);
    // let data = JSON.parse(localStorage.getItem("datas"));

  }
  selectChangeHandler2(event: any) {
    if (event.target) {
      this.brewerSelected2 = event.target.value;
    } else {
      this.brewerSelected2 = event;
    }
    this.beerInfo2 = [];
    for (let i = 0; i < this.Beers.length; i++) {
      if (this.Beers[i].brewer === this.brewerSelected2) {
        this.beerInfo2.push({
          price: this.Beers[i].price,
          name: this.Beers[i].name,
          image_url: this.Beers[i].image_url,
          type: this.Beers[i].type,
        }
        )
      }
    }
    this.beerInfo2 = this.beerInfo2.sort(this.dynamicSort("name"));
    this.beerInfoSliced2 = this.beerInfo2.slice(0, this.currentIndex2);
    this.showeHideBtn('btn2', this.beerInfoSliced2.length, this.beerInfo2.length);
    localStorage.removeItem("brewerSelected2");
    localStorage.setItem("brewerSelected2", this.brewerSelected2);


  }
  selectChangeHandler3(event: any) {
    if (event.target) {
      this.brewerSelected3 = event.target.value;
    } else {
      this.brewerSelected3 = event;
    }
    this.beerInfo3 = [];
    for (let i = 0; i < this.Beers.length; i++) {
      if (this.Beers[i].brewer === this.brewerSelected3) {
        this.beerInfo3.push({
          price: this.Beers[i].price,
          name: this.Beers[i].name,
          image_url: this.Beers[i].image_url,
          type: this.Beers[i].type,
        }
        )
      }
    }
    this.beerInfo3 = this.beerInfo3.sort(this.dynamicSort("name"));
    this.beerInfoSliced3 = this.beerInfo3.slice(0, this.currentIndex3)
    this.showeHideBtn('btn3', this.beerInfoSliced3.length, this.beerInfo3.length)
    localStorage.removeItem("brewerSelected3");
    localStorage.setItem("brewerSelected3", this.brewerSelected3);

  }
  loadMore(num: number) {
    let maxLen = 0;
    if (num === 1) {
      maxLen = this.beerInfo1.length;
      if (maxLen > 15) {
        if (this.currentIndex1 + 15 <= maxLen) {
          this.currentIndex1 = this.currentIndex1 + 15
        } else {
          this.currentIndex1 = maxLen;
        }
        if (this.currentIndex1 < maxLen) {
          this.currentIndex1;
        } else {
          this.currentIndex1 = maxLen;
        }

      }

      for (let i = this.beerInfoSliced1.length; i < this.currentIndex1; i++) {

        this.beerInfoSliced1.push({
          price: this.beerInfo1[i].price,
          name: this.beerInfo1[i].name,
          image_url: this.beerInfo1[i].image_url,
          type: this.beerInfo1[i].type,
        }
        )
      }

    } else if (num === 2) {
      maxLen = this.beerInfo2.length;
      if (maxLen > 15) {
        this.currentIndex2 = this.currentIndex2 + 15
        if (this.currentIndex2 < maxLen) {
          this.currentIndex2;
        } else {
          this.currentIndex2 = maxLen;
        }
      }
      for (let i = this.beerInfoSliced2.length; i < this.currentIndex2; i++) {
        this.beerInfoSliced2.push({
          price: this.beerInfo2[i].price,
          name: this.beerInfo2[i].name,
          image_url: this.beerInfo2[i].image_url,
          type: this.beerInfo2[i].type,
        }
        )
      }
    } else if (num === 3) {
      maxLen = this.beerInfo3.length;
      if (maxLen > 15) {
        this.currentIndex3 = this.currentIndex3 + 15

        if (this.currentIndex3 < maxLen) {
          this.currentIndex3;
        } else {
          this.currentIndex3 = maxLen;
        }
      }
      for (let i = this.beerInfoSliced3.length; i < this.currentIndex3; i++) {
        this.beerInfoSliced3.push({
          price: this.beerInfo3[i].price,
          name: this.beerInfo3[i].name,
          image_url: this.beerInfo3[i].image_url,
          type: this.beerInfo3[i].type,
        }
        )

      }
    }
    if (num === 1) {
      this.beerInfoSliced1 = this.beerInfoSliced1.sort(this.dynamicSort("name"));
      this.beerInfoSliced1 = this.beerInfoSliced1.slice(0, this.currentIndex1);
      this.showeHideBtn('btn1', this.beerInfoSliced1.length, this.beerInfo1.length)

    } else if (num === 2) {
      this.beerInfoSliced2 = this.beerInfoSliced2.sort(this.dynamicSort("name"));
      this.beerInfoSliced2 = this.beerInfoSliced2.slice(0, this.currentIndex2);
      this.showeHideBtn('btn2', this.beerInfoSliced2.length, this.beerInfo2.length)

    } else if (num === 3) {
      this.beerInfoSliced3 = this.beerInfoSliced3.sort(this.dynamicSort("name"));
      this.beerInfoSliced3 = this.beerInfoSliced3.slice(0, this.currentIndex3);
      this.showeHideBtn('btn3', this.beerInfoSliced3.length, this.beerInfo3.length)


    }
  }

  open(section: number, nr: number) {
    if (section === 1) {
      var modal = document.getElementById("myModal");
      var modalImg = document.getElementById("img01");
      modal.style.display = "block";
      modalImg.setAttribute('src', this.beerInfoSliced1[nr].image_url)
      this.testImage(this.beerInfoSliced1[nr].image_url)
      var span = document.getElementById("close");
      span.onclick = function () { modal.style.display = "none" };
    } else if (section === 2) {
      var modal = document.getElementById("myModal2");
      var modalImg2 = document.getElementById("img02");
      modal.style.display = "block";
      modalImg2.setAttribute('src', this.beerInfoSliced2[nr].image_url)
      this.testImage(this.beerInfoSliced2[nr].image_url)
      var span = document.getElementById("close2");
      span.onclick = function () { modal.style.display = "none" };
    } else if (section === 3) {
      var modal = document.getElementById("myModal3");
      var modalImg3 = document.getElementById("img03");
      modal.style.display = "block";
      modalImg3.setAttribute('src', this.beerInfoSliced3[nr].image_url)
      this.testImage(this.beerInfoSliced3[nr].image_url)
      var span = document.getElementById("close3");
      span.onclick = function () { modal.style.display = "none" };
    }
  }

  testImage(URL: string) {
    var tester = new Image();
    tester.onerror = this.imageNotFound;
    tester.src = URL;

  }

  imageNotFound() {
    var modalImg1 = document.getElementById("img01");
    modalImg1.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/450px-No_image_available.svg.png')
    var modalImg2 = document.getElementById("img02");
    modalImg2.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/450px-No_image_available.svg.png')
    var modalImg3 = document.getElementById("img03");
    modalImg3.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/450px-No_image_available.svg.png')

  }
}
