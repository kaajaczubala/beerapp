import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
interface myData {
  obj: Array<Object>;
}
@Injectable({
  providedIn: 'root'
})

export class GetApiService {

  constructor(private http: HttpClient) { }
  apiCall() {
    return this.http.get('http://ontariobeerapi.ca/beers');
  }
}
